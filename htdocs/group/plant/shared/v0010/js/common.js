var mg = mg || {
	util: {},
	func: {}
};

/**
オブジェクトのマージ
*/
mg.util.extend = function() {
	var obj,
		result = {},
		len = arguments.length;

	for(var i = 0; i < len; i++) {
		obj = arguments[i];
		for (var p in obj) {
			if ( obj.hasOwnProperty(p) ) {
				result[p] = obj[p];
			}
		}
	}
	return result;
};

/**
指定したCSSプロパティが使用可能かテスト
*/
mg.util.testCSSProp = function (prop) {
	var prefixes = ['Webkit', 'Moz', 'O', 'ms'],
		ucProp = prop.charAt(0).toUpperCase() + prop.slice(1),
        props = (prop + ' ' + prefixes.join(ucProp + ' ') + ucProp).split(' ');

	for ( var i in props ) {
		if ( document.createElement('div').style[props[i]] !== undefined ) {
			return true;
		}
	}
	return false;
}

/**
指定した要素の位置情報を返す
*/
mg.util.getOffset = function(el) {
	var rect = el.getBoundingClientRect();
	return {
		left: rect.left + window.pageXOffset,
		top: rect.top + window.pageYOffset
	}
}

/**
アコーディオン
*/
mg.func.Accordion = function (_elemTrigger, _elemContent, option) {
	var Accordion = this;
	var _defs;
	var _setting;
	var _isOpen;
	var _h;
	var _timerid;
	var _csstransitions = mg.util.testCSSProp('transition');

	_defs = {
		activeClass : 'opened',
		// labelClose : '閉じる',
		opened : false
	}

	var init = function (){
		_setting = mg.util.extend(_defs, option);
		_isOpen = _setting.opened;
		if(!_elemTrigger || !_elemContent) {
			return false;
		}

		Accordion.enable();
	};

	function onTriggerClick (evt){
		evt.preventDefault();

		if(!_isOpen){
			Accordion.onOpen();
		} else {
			Accordion.onClose();
		}
	}

	function onOpenEnd () {
		_elemContent.style.height= 'auto';
		window.addEventListener('orientationchange', refreshHeight, false);
		window.addEventListener('resize', refreshHeight, false);
	}

	function onCloseEnd () {
		_elemContent.removeAttribute('style');
	}

	function refreshHeight () {
		_h = _elemContent.offsetHeight;
	}

	function addTransitionEndEvent (elem, callback) {
		if (_csstransitions) {
			elem.addEventListener('webkitTransitionEnd', callback, false);
			elem.addEventListener('MozTransitionEnd', callback, false);
			elem.addEventListener('mozTransitionEnd', callback, false);
			elem.addEventListener('msTransitionEnd', callback, false);
			elem.addEventListener('oTransitionEnd', callback, false);
			elem.addEventListener('transitionEnd', callback, false);
			elem.addEventListener('transitionend', callback, false);
		}
		else {
			callback();
		}
	}

	function removeTransitionEndEvent (elem, callback) {
		if (_csstransitions) {
			elem.removeEventListener('webkitTransitionEnd', callback, false);
			elem.removeEventListener('MozTransitionEnd', callback, false);
			elem.removeEventListener('mozTransitionEnd', callback, false);
			elem.removeEventListener('msTransitionEnd', callback, false);
			elem.removeEventListener('oTransitionEnd', callback, false);
			elem.removeEventListener('transitionEnd', callback, false);
			elem.removeEventListener('transitionend', callback, false);
		}
		else {
			callback();
		}
	}

	var getHeight = function(elem) {
		var temp = {};
		var h;
		temp.opacity = elem.style.opacity;
		temp.visibility = elem.style.visibility;
		temp.height = elem.style.height;

		elem.style.display = 'block';
		elem.style.opacity = 0;
		elem.style.visibility = 'hidden';
		elem.style.height = 'auto';
		h = elem.offsetHeight;
		elem.style.opacity = temp.opacity;
		elem.style.visibility = temp.visibility;
		elem.style.height = temp.height;

		return h;
	};

	Accordion.isOpen = function () {
		return _isOpen;
	};

	Accordion.onOpen = function () {
		clearTimeout(_timerid);
		removeTransitionEndEvent(_elemContent, onCloseEnd);

		if ( _setting.onOpen && typeof _setting.onOpen == 'function' ) {
			_setting.onOpen();
		}

		_elemTrigger.classList.add(_setting.activeClass);
		_elemContent.classList.add(_setting.activeClass);
		_h = getHeight(_elemContent);

		_timerid = setTimeout(function () {
			_elemContent.style.height = _h + 'px';
			addTransitionEndEvent(_elemContent, onOpenEnd);
			_isOpen = true;
		}, 10);
	};

	Accordion.onClose = function () {
		clearTimeout(_timerid);
		window.removeEventListener ('orientationchange', refreshHeight);
		window.removeEventListener ('resize', refreshHeight);
		removeTransitionEndEvent(_elemContent, onOpenEnd);

		if ( _setting.onClose && typeof _setting.onClose == 'function' ) {
			_setting.onClose();
		}

		_elemContent.style.height = _h + 'px';
		_elemTrigger.classList.remove(_setting.activeClass);
		_elemContent.classList.remove(_setting.activeClass);

		_timerid = setTimeout(function () {
			_elemContent.style.height = 0;
			addTransitionEndEvent(_elemContent, onCloseEnd);
			_isOpen = false;
		}, 10);
	};

	Accordion.enable = function() {
		// Accordion.disable();
		_elemTrigger.addEventListener('click', onTriggerClick, false);
	};

	Accordion.disable = function() {
		if (_isOpen) {
			if ( _setting.onClose && typeof _setting.onClose == 'function' ) {
				_setting.onClose();
			}
			window.removeEventListener ('orientationchange', refreshHeight);
			window.removeEventListener ('resize', refreshHeight);
			removeTransitionEndEvent(_elemContent, onOpenEnd);
			_elemTrigger.classList.remove(_setting.activeClass);
			_elemContent.classList.remove(_setting.activeClass);
			_elemContent.removeAttribute('style');
		}
		_elemTrigger.removeEventListener('click', onTriggerClick, false);
		_isOpen = false;
	};

	init();

	return Accordion;
}

/**
グローバルナビの設定
*/
mg.initGlobalNav = function () {
	//グローバルナビのアコーディオン設定
	var trigger = document.querySelector('#global_header .btn_menu');
	var body = document.querySelector('#global_nav');
	var accordion;

	if (!trigger || !body) {
		return false;
	}

	accordion = new mg.func.Accordion(trigger, body, {
		onOpen: function() {
			trigger.querySelector('.inner').innerHTML = '閉じる';
		},
		onClose: function() {
			trigger.querySelector('.inner').innerHTML = 'メニュー';
		}
	});

	var mq = window.matchMedia('screen and (max-width: 767px)');
	mq.addListener(checkBreakPoint);
	checkBreakPoint(mq);
	function checkBreakPoint(mq) {
		if (mq.matches) {
			accordion.enable();
		} else {
			accordion.disable();
		}
	}
};

/**
スキップナビの設定
*/
mg.initSkipNav = function() {
	var $trigger = document.querySelector('#skipnav');

	if ( !$trigger ) {
		return false;
	}
	var targetId = $trigger.getAttribute('href');
	var $target = document.querySelector(targetId);
	var timerId;

	if ( !$target ) {
		return false;
	}
	//フォーカス可能にする
	$target.tabIndex = -1;

	//スキップナビを閉じた後にスクロールさせる
	var skipToContent = function() {
		clearTimeout(timerId);
		$trigger.blur();
		timerId = setTimeout(function() { $target.focus(); location.hash = targetId; }, 100);
	};

	//mousedownはMac safari対策
	//keydownはjaws+ieで発生しない
	$trigger.addEventListener( 'mousedown', skipToContent, false );
	$trigger.addEventListener( 'click', skipToContent, false );
};

/**
ページトップの設定
*/
mg.initPageTop = function () {
	var elemButton = document.querySelector( '#pagetop a' );
	var elemTrigger = document.querySelector('#global_footer');
	var isPinned = false;

	if (!elemButton) {
		return false;
	}

	elemButton.addEventListener('click', function(evt) {
		evt.preventDefault();
		scrollToTop();
	}, false);

	window.addEventListener('scroll', function() {
		if ( getScrolled() > 500 ) {
			elemButton.classList.add('show', 'pin');
			isPinned = true;
		} else {
			elemButton.classList.remove('show', 'pin');
			isPinned = false;
		}
	}, false);

	if (elemTrigger) {
		window.addEventListener('scroll', function() {
			var triggerPos = mg.util.getOffset(elemTrigger).top;
			var winH = window.innerHeight;
			if (isPinned && triggerPos < (getScrolled() + winH)) {
				elemButton.classList.remove('pin');
				isPinned = false;
			} else if(!isPinned && triggerPos > (getScrolled() + winH)) {
				elemButton.classList.add('pin');
			}
		}, false);
	}

	function getScrolled() {
		return ( window.pageYOffset !== undefined ) ? window.pageYOffset: document.documentElement.scrollTop;
	}

	function scrollToTop () {
		var scrolled = getScrolled();
		window.scrollTo( 0, Math.floor( scrolled / 1.3 ) );
		if ( scrolled > 0 ) {
			setTimeout( scrollToTop, 30 );
		} else {
			focusPageTop();
		}
	}

	function focusPageTop() {
		var elemTarget = document.querySelector(elemButton.getAttribute('href'));

		if ( !elemTarget ) {
			return false;
		}
		elemTarget.tabIndex = -1;
		elemTarget.focus();
	}
};

/**
画面読み込み時の処理
*/
window.addEventListener('DOMContentLoaded', function(){
	mg.initGlobalNav();
	mg.initSkipNav();
	mg.initPageTop();
}, false);

